# `captn ds`

A set of commands for managing datasources and training ML models on them.

**Usage**:

```console
$ captn ds [OPTIONS] COMMAND [ARGS]...
```

**Options**:

* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.

**Commands**:

* `details`: Return details of a datasource.
* `dtypes`: Return the dtypes of the datasource.
* `head`: Return the first few rows of the datasource.
* `ls`: Return the list of datasources.
* `rm`: Delete a datasource from the server.
* `tag`: Tag an existing datasource in server.
* `train`: Train a model against the datasource.

## `captn ds details`

Return details of a datasource.

**Usage**:

```console
$ captn ds details [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Datasource uuid.  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds dtypes`

Return the dtypes of the datasource.

**Usage**:

```console
$ captn ds dtypes [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Datasource uuid.  [required]

**Options**:

* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds head`

Return the first few rows of the datasource.

**Usage**:

```console
$ captn ds head [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Datasource uuid.  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds ls`

Return the list of datasources.

**Usage**:

```console
$ captn ds ls [OPTIONS]
```

**Options**:

* `-o, --offset INTEGER`: The number of datasources to offset at the beginning. If **None**, then the default value **0** will be used.  [default: 0]
* `-l, --limit INTEGER`: The maximum number of datasources to return from the server. If **None**, then the default value **100** will be used.  [default: 100]
* `--disabled`: If set to **True**, then only the deleted datasources will be returned.Else, the default value **False** will be used to return only the listof active datasources.
* `--completed`: If set to **True**, then only the datasources that are successfully downloadedto the server will be returned. Else, the default value **False** will be used toreturn all the datasources.
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output only datasource uuids separated by space
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds rm`

Delete a datasource from the server.

**Usage**:

```console
$ captn ds rm [OPTIONS] UUID
```

**Arguments**:

* `UUID`: Datasource uuid.  [required]

**Options**:

* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-q, --quiet`: Output the deleted datasource uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds tag`

Tag an existing datasource in server.

**Usage**:

```console
$ captn ds tag [OPTIONS]
```

**Options**:

* `-uuid, --datasource_uuid TEXT`: Datasource uuid.  [required]
* `-n, --name TEXT`: A string to tag the datasource.  [required]
* `-f, --format TEXT`: Format output and show only the given column(s) values.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.

## `captn ds train`

Train a model against the datasource.

**Usage**:

```console
$ captn ds train [OPTIONS]
```

**Options**:

* `-uuid, --datasource_uuid TEXT`: Datasource uuid.  [required]
* `--client_column TEXT`: The column name that uniquely identifies the users/clients.  [required]
* `--timestamp_column TEXT`: The timestamp column indicating the time of an event. If not passed, then the default value **None** will be used. 
* `--target_column TEXT`: Target column name that indicates the type of the event.  [required]
* `--target TEXT`: Target event name to train and make predictions. You can pass the target event as a string or as a regular expression for predicting more than one event. For example, passing ***checkout** will train a model to predict any checkout event.  [required]
* `--predict_after TEXT`: Time delta in hours of the expected target event.  [required]
* `-q, --quiet`: Output model uuid only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--help`: Show this message and exit.
