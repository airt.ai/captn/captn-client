# `captn token`

Get application token for captn service from a username/password pair.

To access the captn service, you must first create a developer account. To obtain one, please contact us at info@captn.ai.

After successful verification, you will receive an email with the username and password for the developer account.

Once you have the credentials, use them to get an access token by running **captn token** command. It is necessary to
get an access token; otherwise, you won't be able to access all of the captn service's APIs. You can either pass the
username, password, and server address as command line arguments or store them in the environment variables
**CAPTN_SERVICE_USERNAME**, **CAPTN_SERVICE_PASSWORD**, and **CAPTN_SERVER_URL**.

If you've already enabled multi-factor authentication (MFA) for your account, you'll need to pass the dynamically
generated six-digit verification code along with your username and password to generate new tokens.

If the token is requested using Single sign-on (SSO), an authorization URL will be returned. Please copy and paste
it into your preferred browser and complete the SSO provider authentication within 10 minutes. Otherwise,
the SSO login will time out and you will need to re-request the token.

Single sign-on (SSO) can be enabled for your account in three simple steps:

1. Enable the SSO for a provider by calling the command `captn user sso enable` with the SSO provider name and an email address.
At the moment, we only support "google" and "github" as SSO providers. We intend to support additional SSO providers in future releases.

2. Before you can start generating new tokens with SSO, you must first authenticate with the SSO provider. Call the `captn token` command with
the same SSO provider you have enabled in the step above to generate an SSO authorization URL. Please copy and paste it into your
preferred browser and complete the authentication process with the SSO provider.

3. After successfully authenticating with the SSO provider, an access token will be generated and returned. Please set it in the
**CAPTN_SERVICE_TOKEN** environment variable for accessing the captn service.

**Usage**:

```console
$ captn token [OPTIONS]
```

**Options**:

* `-u, --username TEXT`: Username for the developer account. If None (default value), then the value from **CAPTN_SERVICE_USERNAME** environment variable is used.
* `-p, --password TEXT`: Password for the developer account. If None (default value), then the value from **CAPTN_SERVICE_PASSWORD** environment variable is used.
* `-s, --server TEXT`: The captn server uri. If None (default value), then the value from **CAPTN_SERVER_URL** environment variable is used. If the variable is not set as well, then the default public server will be used. Please leave this setting to default unless you are running the service in your own server (please email us to info@captn.ai for that possibility).
* `--otp TEXT`: Dynamically generated six-digit verification code from the authenticator app or the OTP you have received via SMS. Please do not pass this parameter if you haven't enabled the multi-factor authentication for your account.
* `-sp, --sso_provider TEXT`: Name of the Single sign-on (SSO) provider. At the moment, we only support google and github as SSO providers. Please pass this parameter only if you have successfully enabled SSO for the provider.
* `-q, --quiet`: Output authentication token only.
* `-d, --debug`: Set logger level to DEBUG and output everything.
* `--install-completion [bash|zsh|fish|powershell|pwsh]`: Install completion for the specified shell.
* `--show-completion [bash|zsh|fish|powershell|pwsh]`: Show completion for the specified shell, to copy it or customize the installation.
* `--help`: Show this message and exit.
