# Tutorial

This tutorial gives an example of how to use captn services to train a model and make predictions.

We can use the **captn-client** library's following classes for the task at hand:

- `Client` for authenticating and accessing the captn service,

- `DataBlob` for encapsulating the data from sources like CSV files, databases, or AWS S3 bucket, and

- `DataSource` for managing datasources and training the models in the captn service.

We import them from **captn.client** module as follows:


```
# Importing necessary libraries
from captn.client import Client, DataBlob, DataSource
```

### Authentication 

To access the captn service, you must first create a developer account. Please fill out the signup form below to get one:

- [https://bit.ly/3I4cNuv](https://bit.ly/3I4cNuv)

After successful verification, you will receive an email with the username and password for the developer account.

Once you have the credentials, use them to get an access token by calling `Client.get_token` method. It is necessary to get an access token; otherwise, you won't be able to access all of the captn service's APIs. You can either pass the username, password, and server address as parameters to the `Client.get_token` method or store them in the environment variables **CAPTN_SERVICE_USERNAME**, **CAPTN_SERVICE_PASSWORD**, and **CAPTN_SERVER_URL**.

In addition to the regular authentication with credentials, you can also enable multi-factor authentication (MFA) and single sign-on (SSO) for generating tokens.

To help protect your account, we recommend that you enable multi-factor authentication (MFA). MFA provides additional security by requiring you to provide unique verification code (OTP) in addition to your regular sign-in credentials when performing critical operations.

Your account can be configured for MFA in just two easy steps:

1. To begin, you need to enable MFA for your account by calling the `User.enable_mfa` method, which will generate a QR code. You can then 
scan the QR code with an authenticator app, such as Google Authenticator and follow the on-device instructions to finish the setup in your smartphone.

2. Finally, activate MFA for your account by calling `User.activate_mfa` and passing the dynamically generated six-digit verification code from your 
smartphone's authenticator app.

You can also disable MFA for your account at any time by calling the method `User.disable_mfa` method.

Single sign-on (SSO) can be enabled for your account in three simple steps:

1. Enable the SSO for a provider by calling the `User.enable_sso` method with the SSO provider name and an email address. At the moment, 
we only support "google" and "github" as SSO providers. We intend to support additional SSO providers in future releases.

2. Before you can start generating new tokens with SSO, you must first authenticate with the SSO provider. Call the `Client.get_token` with 
the same SSO provider you have enabled in the step above to generate an SSO authorization URL. Please copy and paste it into your 
preferred browser and complete the authentication process with the SSO provider.

3. After successfully authenticating with the SSO provider, call the `Client.set_sso_token` method to generate a new token and use it automatically 
in all future interactions with the captn server.

!!! info

	In the below example, the username, password, and server address are stored in **CAPTN_SERVICE_USERNAME**, **CAPTN_SERVICE_PASSWORD**, and **CAPTN_SERVER_URL** environment variables.



```
# Authenticate
Client.get_token()
```

### 1. Connect and preprocess data

`DataBlob` objects are used to encapsulate data access. Currently, we support:

- access for local CSV files,

- database access for MySql, ClickHouse, and 

- files stored in cloud storage like AWS S3 and Azure Blob Storage.

We intend to support additional databases and storage mediums in future releases.

To create a `DataBlob` object, use one of the DataBlob class's from_* methods. Check out the `DataBlob` class documentation for more information.

In this example, the input data is a CSV file stored in an AWS S3 bucket. Before you can use the data to train a model, it must be uploaded to the captn server.
To upload data from an AWS S3 bucket to the captn server, use the DataBlob class's `DataBlob.from_s3` method.

In our example, we will be using the captn APIs to load and preprocess a sample CSV file stored in an AWS S3 bucket. 


```
# Pull the data from an AWS S3 bucket to the captn server
data_blob = DataBlob.from_s3(
    uri="s3://test-airt-service/sample_gaming_130k"
)
```

The above method will automatically pull the data into the captn server, and all calls to the library are asynchronous and return immediately. To manage completion, all the **from_*** methods of the DataBlob class will return a status object indicating the completion status. Alternatively, you can monitor the completion status interactively in a progress bar by calling the `DataBlob.progress_bar` method:


```
data_blob.progress_bar()
```

    100%|██████████| 1/1 [00:30<00:00, 30.34s/it]


The next step is to preprocess and prepare the data for training. Preprocessing entails creating the index column, sort column, and so on. Currently, CSV and Parquet files can be preprocessed. Please use the `DataBlob.to_datasource` method in the DataBlob class for the same. We intend to support additional file formats in the future releases.

The sample data we used in this example doesn't have the header rows and their data types defined. 

The following code creates the necessary headers along with their data types and reads only a subset of columns that are required for modeling:



```
# Add header rows
prefix = ["revenue", "ad_revenue", "conversion", "retention"]
days = list(range(30)) + list(range(30, 361, 30))
dtype = {
    "date": "str",
    "game_name": "str",
    "platform": "str",
    "user_type": "str",
    "network": "str",
    "campaign": "str",
    "adgroup": "str",
    "installs": "int32",
    "spend": "float32",
}
dtype.update({f"{p}_{d}": "float32" for p in prefix for d in days})
names = list(dtype.keys())

kwargs = {"delimiter": "|", "names": names, "parse_dates": ["date"], "usecols": names[:42], "dtype": dtype}
```

Finally, the above variables are passed to the `DataBlob.to_datasource` method which preprocesses the data and stores it in captn server.


```
# Preprocess and prepare the data for training
data_source = data_blob.to_datasource(
    file_type="csv",
    index_column="game_name",
    sort_by="date",
    **kwargs
)

# Display the data preprocessing progress
data_source.progress_bar()
```

    100%|██████████| 1/1 [00:50<00:00, 50.59s/it]


When the preprocessing is finished, you can run the following command to display the head of the data to ensure everything is fine.


```
# Display the first few rows of preprocessed data.
data_source.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>date</th>
      <th>platform</th>
      <th>user_type</th>
      <th>network</th>
      <th>campaign</th>
      <th>adgroup</th>
      <th>installs</th>
      <th>spend</th>
      <th>revenue_0</th>
      <th>revenue_1</th>
      <th>...</th>
      <th>revenue_23</th>
      <th>revenue_24</th>
      <th>revenue_25</th>
      <th>revenue_26</th>
      <th>revenue_27</th>
      <th>revenue_28</th>
      <th>revenue_29</th>
      <th>revenue_30</th>
      <th>revenue_60</th>
      <th>revenue_90</th>
    </tr>
    <tr>
      <th>game_name</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>ios</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_541</td>
      <td>1</td>
      <td>0.600000</td>
      <td>0.000000</td>
      <td>0.018173</td>
      <td>...</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
      <td>0.018173</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>ios</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_2351</td>
      <td>2</td>
      <td>4.900000</td>
      <td>0.000000</td>
      <td>0.034000</td>
      <td>...</td>
      <td>0.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>6.034000</td>
      <td>13.030497</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>ios</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_636</td>
      <td>3</td>
      <td>7.350000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
      <td>12.112897</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>ios</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_569</td>
      <td>1</td>
      <td>0.750000</td>
      <td>0.000000</td>
      <td>0.029673</td>
      <td>...</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
      <td>0.029673</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>ios</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_243</td>
      <td>2</td>
      <td>3.440000</td>
      <td>0.000000</td>
      <td>0.027981</td>
      <td>...</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
      <td>0.042155</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>android</td>
      <td>googleadwords_int</td>
      <td>googleadwords_int</td>
      <td>campaign_283</td>
      <td>adgroup_1685</td>
      <td>11</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.097342</td>
      <td>...</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
      <td>0.139581</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>android</td>
      <td>googleadwords_int</td>
      <td>googleadwords_int</td>
      <td>campaign_2</td>
      <td>adgroup_56</td>
      <td>32</td>
      <td>30.090000</td>
      <td>0.000000</td>
      <td>0.802349</td>
      <td>...</td>
      <td>2.548253</td>
      <td>2.548253</td>
      <td>2.771138</td>
      <td>2.805776</td>
      <td>2.805776</td>
      <td>2.805776</td>
      <td>2.805776</td>
      <td>2.805776</td>
      <td>2.805776</td>
      <td>2.805776</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>android</td>
      <td>moloco_int</td>
      <td>moloco_int</td>
      <td>campaign_191</td>
      <td>None</td>
      <td>291</td>
      <td>503.480011</td>
      <td>34.701553</td>
      <td>63.618111</td>
      <td>...</td>
      <td>116.508331</td>
      <td>117.334709</td>
      <td>117.387489</td>
      <td>117.509506</td>
      <td>118.811417</td>
      <td>118.760765</td>
      <td>119.151291</td>
      <td>119.350220</td>
      <td>139.069443</td>
      <td>147.528793</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>android</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_190</td>
      <td>4</td>
      <td>2.740000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>game_name_0</th>
      <td>2021-03-15</td>
      <td>android</td>
      <td>jetfuelit_int</td>
      <td>jetfuelit_int</td>
      <td>campaign_0</td>
      <td>adgroup_755</td>
      <td>8</td>
      <td>11.300000</td>
      <td>13.976003</td>
      <td>14.358793</td>
      <td>...</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
      <td>14.338905</td>
    </tr>
  </tbody>
</table>
<p>10 rows × 41 columns</p>
</div>



### 2. Training 


```
# Todo
```
