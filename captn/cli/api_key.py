# AUTOGENERATED! DO NOT EDIT! File to edit: notebooks/CLI_ApiKey.ipynb (unless otherwise specified).

__all__ = ["app"]

# Cell

import airt
from ..client import _fix_cli_doc_string

# Cell

from airt.cli.api_key import app as _app

app = _app

_fix_cli_doc_string(app, "api_key")
